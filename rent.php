<?php
if($_SERVER['REQUEST_METHOD'] !== 'POST' || !isset($_POST['from'], $_POST['to']))
{
    ?><script>alert("잘못된 접근입니다."); history.back();</script><?php
    exit();
}
$store = 0;
if(isset($_POST['from']))
{
    $store = intval($_POST['from']);
}
if(!$store)
{
    echo "올바르지 않은 지점입니다.";
    exit();
}
$store_to = 0;
if(isset($_POST['to']))
{
    $store_to = intval($_POST['to']);
}
if(!$store_to)
{
    echo "올바르지 않은 지점입니다.";
    exit();
}
include ".htdbconfig.php";
if(!$result = $conn->query("SELECT COUNT(*) FROM acid_store WHERE seq = $store_to"))
{
    echo "없는 지점입니다.";
    $conn->close();
    exit();
}
if(!$conn->query("START TRANSACTION"))
{
    echo "서버 오류. 관리자에게 문의 바랍니다.";
    $conn->close();
    exit();
}
$arr = Array();
foreach($_POST as $k => $v)
{
    if(strpos($k, "item_") === 0)
    {
        $tmp = intval(substr($k, 5));
        if($tmp)
        {
            $arr[] = $tmp;
        }
    }
}
$arr = implode(", ", $arr);
$result = $conn->query("SELECT item, cnt FROM acid_stock WHERE store = $store AND item IN ($arr) FOR UPDATE");
if(!$result)
{
    echo "오류가 발생하였습니다.";
    $conn->query("ROLLBACK");
    $conn->close();
    exit();
}
$counts = Array();
while($row = $result->fetch_assoc())
{
    $counts[$row['item']] = intval($_POST["item_$row[item]"]);
    if($counts[$row['item']] > $row['cnt'])
    {
        $conn->query("ROLLBACK");
        $conn->close();
        echo "재고가 부족합니다!";
        exit();
    }
}
$result->close();
if(!$conn->query("INSERT INTO acid_log (store_from, store_to) VALUES ($store, $store_to)"))
{
    $conn->query("ROLLBACK");
    $conn->close();
    echo "대여를 실패했습니다!";
    exit();
}
$id = $conn->insert_id;
$stmt_stock = $conn->prepare("UPDATE acid_stock SET cnt = cnt - ? WHERE store = $store AND item = ?");
if(!$stmt_stock)
{
    echo "서버 오류! 관리자에게 문의 바랍니다.";
    exit();
}
$stmt_item = $conn->prepare("INSERT INTO acid_item (seq, cnt, item) VALUES ($id, ?, ?)");
if(!$stmt_item)
{
    echo "서버 오류! 관리자에게 문의 바랍니다.";
    exit();
}
foreach($counts as $k => $v)
{
    if(!$stmt_stock->bind_param("ii", $v, $k))
    {
        $conn->query("ROLLBACK");
        $stmt_stock->close();
        $stmt_item->close();
        $conn->close();
        echo "대여 중 오류가 발생하였습니다.";
        exit();
    }
    if(!$stmt_stock->execute())
    {
        $conn->query("ROLLBACK");
        $stmt_stock->close();
        $stmt_item->close();
        $conn->close();
        echo "대여 중 오류가 발생하였습니다!";
        exit();
    }
    if(!$stmt_item->bind_param("ii", $v, $k))
    {
        $conn->query("ROLLBACK");
        $stmt_stock->close();
        $stmt_item->close();
        $conn->close();
        echo "대여 중 오류가 발생하였습니다.";
        exit();
    }
    if(!$stmt_item->execute())
    {
        $conn->query("ROLLBACK");
        $stmt_stock->close();
        $stmt_item->close();
        $conn->close();
        echo "대여 중 오류가 발생하였습니다!";
        exit();
    }
}
$conn->query("COMMIT");
$stmt_stock->close();
$stmt_item->close();
$conn->close();
echo "대여 성공!";
?>