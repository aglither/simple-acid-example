-- 데이터베이스와 사용자가 없는 경우에만 (적당히 바꿔서) 실행해 주세요.
-- MariaDB 기준으로 작성된 쿼리입니다. 데이터베이스에 따라 적당히 바꿔주세요.
-- CREATE DATABASE php;
-- GRANT ALL PRIVILEGES ON php.* TO 'php'@'%' IDENTIFIED BY 'phppassword';
-- USE php;

CREATE TABLE acid_items
(
    seq INT PRIMARY KEY AUTO_INCREMENT,
    item_name VARCHAR(255) UNIQUE NOT NULL
);

CREATE TABLE acid_store
(
    seq INT PRIMARY KEY AUTO_INCREMENT,
    store_name VARCHAR(255) UNIQUE NOT NULL
);

CREATE TABLE acid_stock
(
    store INT,
    item INT,
    cnt INT NOT NULL,
    PRIMARY KEY(store, item),
    INDEX(item, store)
) ENGINE = 'InnoDB';

CREATE TABLE acid_log
(
    seq INT PRIMARY KEY AUTO_INCREMENT,
    store_from INT NOT NULL,
    store_to INT NOT NULL,
    start TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    end TIMESTAMP NULL,
    INDEX(end)
) ENGINE = 'InnoDB';

CREATE TABLE acid_item
(
    seq INT NOT NULL,
    item INT NOT NULL,
    cnt INT NOT NULL,
	PRIMARY KEY(seq, item)
);

INSERT INTO acid_items (item_name) VALUES ("item1"), ("item2"), ("item3"), ("item4");

INSERT INTO acid_store (store_name) VALUES ("store1"), ("store2"), ("store3");

INSERT INTO acid_stock (store, item, cnt) VALUES (1, 1, 20), (1, 2, 20), (1, 3, 20), (1, 4, 20);
INSERT INTO acid_stock (store, item, cnt) VALUES (2, 1, 20), (2, 2, 20), (2, 3, 20), (2, 4, 20);
INSERT INTO acid_stock (store, item, cnt) VALUES (3, 1, 20), (3, 2, 20), (3, 3, 20), (3, 4, 20);
