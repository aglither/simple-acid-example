<?php
if(!isset($_GET['id'])) // TODO: Use POST instead of GET
{
    ?><script>alert("잘못된 접근입니다."); history.back();</script><?php
    exit();
}
$id = intval($_GET['id']);
if(!$id)
{
    echo "올바르지 않은 ID입니다.";
    exit();
}
include ".htdbconfig.php";
if(!$conn->query("START TRANSACTION"))
{
    echo "서버 오류. 관리자에게 문의 바랍니다.";
    exit();
}
if(!$result = $conn->query("SELECT store_to FROM acid_log WHERE seq = $id AND end IS NULL FOR UPDATE"))
{
    $conn->close();
    $conn->query("ROLLBACK");
    exit("반납 실패");
}
if(!$row = $result->fetch_assoc())
{
    $conn->query("ROLLBACK");
    $conn->close();
    exit("대여중인 ID가 아닙니다.");
}
$store = $row['store_to'];
$result->close();
if(!$stmt = $conn->prepare("UPDATE acid_stock SET cnt = cnt + ? WHERE store = $store AND item = ?"))
{
    $conn->query("ROLLBACK");
    $conn->close();
    echo "서버 오류! 관리자에게 문의 바랍니다!";
    exit();
}
if(!$result = $conn->query("SELECT item, cnt FROM acid_item WHERE seq = $id"))
{
    $conn->query("ROLLBACK");
    $stmt->close();
    $conn->close();
    echo "반납을 실패했습니다.";
    exit();
}
while($row = $result->fetch_assoc())
{
    if(!$stmt->bind_param("ii", $row['cnt'], $row['item']))
    {
        $conn->query("ROLLBACK");
        $stmt->close();
        $result->close();
        $conn->close();
        echo "서버 오류. 관리자에게 문의 바랍니다.";
        exit();
    }
    if(!$stmt->execute())
    {
        $conn->query("ROLLBACK");
        $stmt->close();
        $result->close();
        $conn->close();
        echo "서버 오류. 관리자에게 문의 바랍니다.";
        exit();
    }
}
$stmt->close();
$result->close();
if(!$result = $conn->query("UPDATE acid_log SET end = CURRENT_TIMESTAMP WHERE seq = $id"))
{
    $conn->query("ROLLBACK");
    $conn->close();
    exit("반납 실패!");
}
$conn->query("COMMIT");
$conn->close();
echo "반납 성공!";
?>